﻿namespace RPGCharacters.Classes
{
    /// <summary>
    /// Stats class that is used by both Heros and Armours since they both focus on same Stats.
    /// </summary>
    public class Stats
    {
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public Stats(int health, int strength, int dexterity, int intelligence)
        {
            Health = health;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
    }
}