﻿using RPGCharacters.Classes.Items;

namespace RPGCharacters.Classes.Weapons
{
    public class Weapon : Item
    {
        public WeaponType Type;
        public int BaseDamage;
        public int TotalDamage;

        public Weapon(string name, int level, WeaponType type, int baseDamage) : base(name, level)
        {
            Type = type;
            BaseDamage = baseDamage;
            CalculateTotalDamage();
        }

        /// <summary>
        /// Calculates total damage that the weapon will have.
        /// </summary>
        private void CalculateTotalDamage()
        {
            TotalDamage = BaseDamage;

            switch (Type)
            {
                case WeaponType.Melee:
                    for (int i = 0; i < Level; i++)
                    {
                        TotalDamage += 2;
                    }
                    break;

                case WeaponType.Ranged:
                    for (int i = 0; i < Level; i++)
                    {
                        TotalDamage += 3;
                    }
                    break;

                case WeaponType.Magic:
                    for (int i = 0; i < Level; i++)
                    {
                        TotalDamage += 2;
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Returns info of the weapon. 
        /// Should not be used directly but through Hero class's GetWeaponInfo()
        /// </summary>
        /// <returns></returns>
        public override string GetInfo()
        {
            return ("Weapon stats for: " + Name + "\n" +
                    "Type: " + Type + "\n" +
                    "Level: " + Level + "\n" +
                    "Base Damage: " + BaseDamage + "\n" +
                    "Total Damage: " + TotalDamage + "\n"
                    );
        }
    }
}