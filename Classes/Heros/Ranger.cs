﻿namespace RPGCharacters.Classes.Heros
{
    internal class Ranger : Hero
    {
        public Ranger(string name, Stats stats) : base(name, stats)
        {
        }

        /// <summary>
        /// Level up stats for ranger.
        /// </summary>
        protected override void LevelUpStats()
        {
            base.Stats.Health += 20;
            base.Stats.Strength += 2;
            base.Stats.Dexterity += 5;
            base.Stats.Intelligence += 1;
        }
    }
}