﻿namespace RPGCharacters.Classes.Heros
{
    /// <summary>
    /// Base Stats for each class. 
    /// Must be used as second parameter when creating Hero. 
    /// Order: Health, Strength, Dexterity, Intelligence
    /// </summary>
    public class HeroBaseStats
    {
        public static Stats GetWarriorStats()
        {
            return new Stats(150, 10, 3, 1);
        }

        public static Stats GetRangerStats()
        {
            return new Stats(120, 5, 10, 2);
        }

        public static Stats GetMageStats()
        {
            return new Stats(100, 2, 3, 10);
        }
    }
}