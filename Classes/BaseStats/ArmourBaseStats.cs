﻿namespace RPGCharacters.Classes.Armours
{
    /// <summary>
    /// Base Stats for types of armours.
    ///  Must be used when creating armours.
    /// </summary>
    public abstract class ArmourBaseStats
    {
        public static Stats GetClothArmourBaseStats()
        {
            return new Stats(10, 0, 1, 3);
        }

        public static Stats GetLeatherArmourBaseStats()
        {
            return new Stats(20, 1, 3, 0);
        }

        public static Stats GetPlateArmourBaseStats()
        {
            return new Stats(30, 3, 1, 0);
        }
    }
}