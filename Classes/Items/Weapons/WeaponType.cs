﻿namespace RPGCharacters.Classes.Weapons
{
    /// <summary>
    /// Enum class for different weapon types.
    /// </summary>
    public enum WeaponType
    {
        Melee,
        Ranged,
        Magic
    }
}