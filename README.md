# RPG Characters
#### Created by Muneeb Rana
## Functionality
Below is the functionality present in the application.
### Heroes
- This application allows users to create heroes (Warriors, Rangers, Mages). These characters are derived from the main Hero class.
- Each hero starts at level 1, and can be leveled up using the LevelUp() method. 
- Heros can equip a single weapon, and multiple armours (max 3). The levels of the weapon and armours must be less or equal to the heroes level or they cannot be equipped.
- Heros have basic stats namely strength, dexterity, health, and intelligence etc. These stats can be increased based on the armour.
### Armours
- Each armour has a name, level, type (cloth, leather, plate) and slot(head, body, legs).
- Level, types and slots affect the overall stats of the armours.
- A hero can equip max 1 armour in each slot (head, body, legs). Trying to equip another before unequipping is not possible. 
- Armours stats are added to the hero's stats when  equipped. 
### Weapons
-   Weapons have a name, level, type (melee, range, magic)
-   Each type has predefined base stats and predefined amounts by which the stats increase when weapon is leveled up.
-   A hero can equip max 1 weapon at one time. Before equipping another weapon, the current weapon must be unequipped.
-   Hero cannot attack if they do not have a weapon.
-   The attack value is defined by the weapons damage and the stats of the hero combined. 
