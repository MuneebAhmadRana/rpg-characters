﻿namespace RPGCharacters.Classes.Armours
{
    /// <summary>
    /// Type enum for different kinds of armours.
    /// </summary>
    public enum ArmourType
    {
        Cloth,
        Leather,
        Plate
    }
}