﻿using RPGCharacters.Classes.Items;

namespace RPGCharacters.Classes.Armours
{
    public class Armour : Item
    {
        public ArmourType Type { get; }
        public ArmourSlot Slot { get; }
        public Stats Stats { get; }

        public Armour(string name, int level, ArmourType type, ArmourSlot slot, Stats stats) : base(name, level)
        {
            Type = type;
            Slot = slot;
            Stats = stats;
            //calculating bonuses and scaling as the armour is instantiated
            CalculateBonuses();
            CalculateScaling();
        }

        /// <summary>
        /// Calculates the bonuses each armour gets per level based on the type of armour 
        /// </summary>
        private void CalculateBonuses()
        {
            switch (Type)
            {
                case ArmourType.Cloth:
                    for (int i = 0; i < Level; i++)
                    {
                        Stats.Health += 5;
                        Stats.Intelligence += 2;
                        Stats.Dexterity += 1;
                    }
                    break;

                case ArmourType.Leather:
                    for (int i = 0; i < Level; i++)
                    {
                        Stats.Health += 8;
                        Stats.Strength += 1;
                        Stats.Dexterity += 2;
                    }
                    break;

                case ArmourType.Plate:
                    for (int i = 0; i < Level; i++)
                    {
                        Stats.Health += 12;
                        Stats.Strength += 2;
                        Stats.Dexterity += 1;
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Calculates scaling based on which body part it is.
        /// </summary>
        private void CalculateScaling()
        {
            switch (Slot)
            {
                //No need to do anything here since scaling is 100%;
                case ArmourSlot.Body:
                    break;

                // 80% scaling
                case ArmourSlot.Head:
                    Stats.Health = (int)(Stats.Health * 0.8);
                    Stats.Strength = (int)(Stats.Strength * 0.8);
                    Stats.Dexterity = (int)(Stats.Dexterity * 0.8);
                    Stats.Intelligence = (int)(Stats.Intelligence * 0.8);
                    break;
                //60% scaling
                case ArmourSlot.Legs:
                    Stats.Health = (int)(Stats.Health * 0.6);
                    Stats.Strength = (int)(Stats.Strength * 0.6);
                    Stats.Dexterity = (int)(Stats.Dexterity * 0.6);
                    Stats.Intelligence = (int)(Stats.Intelligence * 0.6);
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// returns info of the armour. 
        /// </summary>
        /// <returns></returns>
        public override string GetInfo()
        {
            return ("Armour stats for: " + Name + "\n" +
                "Type: " + Type + "\n" +
                "Slot: " + Slot + "\n" +
                "Level: " + Level + "\n" +
                "Bonus HP: " + Stats.Health + "\n" +
                "Bonus Str: " + Stats.Strength + "\n" +
                "Bonus Dex: " + Stats.Dexterity + "\n" +
                "Bonus Int: " + Stats.Intelligence + "\n"
                );
        }
    }
}