﻿namespace RPGCharacters.Classes.Weapons
{
    /// <summary>
    /// Base Damage stats for weapons.
    /// </summary>
    public abstract class WeaponBaseDamage
    {
        public static int GetMeeleeBaseDamage()
        {
            return 15;
        }

        public static int GetRangedBaseDamage()
        {
            return 5;
        }

        public static int GetMagicBaseDamage()
        {
            return 25;
        }
    }
}