﻿using RPGCharacters.Classes.Armours;
using RPGCharacters.Classes.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPGCharacters.Classes.Heros
{
    public abstract class Hero
    {
        private string Name { get; }
        private int Level { get; set; } = 1;
        private int TotalXPNextLevel { get; set; } = 100;
        private int CurrentXP { get; set; } = 0;
        private int Damage { get; set; } = 0;

        public Stats Stats;

        private Weapon Weapon;

        private List<Armour> Armours = new List<Armour>(new Armour[3]);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="stats"></param>
        public Hero(string name, Stats stats)
        {
            Name = name;
            Stats = stats;
        }

        /// <summary>
        /// Returns the basic information of the Hero.
        /// </summary>
        /// <returns></returns>
        public string GetHeroInfo()
        {
            return ("Hero: " + Name + "\n" +
                "HP: " + Stats.Health + "\n" +
                "Str: " + Stats.Strength + "\n" +
                "Dex: " + Stats.Dexterity + "\n" +
                "Int: " + Stats.Intelligence + "\n" +
                "Level: " + Level + "\n" +
                "XP: " + CurrentXP + "/" + TotalXPNextLevel + "\n"
                );
        }

        #region Attack

        /// <summary>
        /// Attack method that is inherited by Warrior, Ranger, and Mage Classes.
        /// If no weapon is equipped, does not attack
        /// Otherwise attacks based on the calculation of Weapon Damage, Stats of Hero and Scaling
        /// </summary>
        /// <returns>
        /// Either No weapon equipped string
        /// Or Damage string
        /// </returns>
        public string Attack()
        {
            if (Weapon == null)
            {
                return "No Weapon Equipped. Hence no attack done.";
            }
            else
            {
                switch (Weapon.Type)
                {
                    case WeaponType.Melee:
                        Damage = (int)(Weapon.TotalDamage + (Stats.Strength * 1.5));
                        break;

                    case WeaponType.Ranged:
                        Damage = (int)(Weapon.TotalDamage + (Stats.Dexterity * 2));
                        break;

                    case WeaponType.Magic:
                        Damage = (int)(Weapon.TotalDamage + (Stats.Dexterity * 3));
                        break;

                    default:
                        break;
                }
                return Damage.ToString();
            }
        }

        #endregion Attack

        #region Level Up Methods

        /// <summary>
        /// Abstract method. Each derived class(warrior, mage, ranger) will have their own implementations
        /// </summary>
        protected abstract void LevelUpStats();

        /// <summary>
        /// LevelUp method levels up the character based on experience parameter.
        /// The level always starts at 1, and the hero can level up again and again. There is no level decrease method.
        /// </summary>
        /// <param name="experience">
        /// Int value that defines how much the Hero wants to level up.
        /// </param>
        public void LevelUp(int experience)
        {
            CurrentXP += experience;
            while (CurrentXP >= TotalXPNextLevel)
            {
                //increasing the level if the experience is bigger than xp
                // Initially and after increasing the xp down below.
                Level++;
                TotalXPNextLevel += XpForumla(Level);
                //setting the total xp required for the next level
                //method of the derived classes (warrior, mage and ranger) are called
                // with their respective levelup values.
                LevelUpStats();
            }
        }

        /// <summary>
        /// Formula that figures out how much Xp is needed per level.
        /// </summary>
        /// <param name="level">
        /// Int value that specifies the level.
        /// </param>
        /// <returns>
        /// Int value that tells how much xp is needed for the specified level.
        /// </returns>
        private int XpForumla(int level)
        {
            return (int)(100 * Math.Pow(1.0 + (10.0 / 100.0), level - 1));
        }

        #endregion Level Up Methods

        #region Weapon Equip, Unequip, GetWeaponInfo

        /// <summary>
        /// Equips Weapon. If weapon level is bigger than Heros level, throws exception
        /// Otheriwse selects the weapon as the new weapon
        /// </summary>
        /// <param name="weapon"></param>
        public string EquipWeapon(Weapon weapon)
        {
            if (Weapon != null)
            {
                return "A weapon is already equipped. Unequip it first.";
            }
            else if (weapon.GetLevel() > Level)
            {
                return "Equipping Weapon failed. Hero's level is lower than weapon's level";
            }
            else
            {
                Weapon = weapon;
                return "Weapon successfully equipped";
            }
        }

        /// <summary>
        /// Unequips the current weapon
        /// </summary>
        public string UnEquipWeapon()
        {
            if (Weapon == null)
            {
                return "No weapon was equipped.";
            }
            else
            {
                Damage = 0;
                Weapon = null;
                return "Weapon unequipped sucessfully";
            }
        }

        /// <summary>
        /// Returns the info for the weapon if a weapon is equipped.
        /// </summary>
        /// <returns></returns>
        public string GetWeaponInfo()
        {
            if (Weapon != null)
            {
                return Weapon.GetInfo();
            }
            else
            {
                return "No weapon is equipped.";
            }
        }

        #endregion Weapon Equip, Unequip, GetWeaponInfo

        #region Armour, Equip, Unequip, Get armours

        /// <summary>
        /// Does necessary checks and equips armour if the checks are successfull.
        /// </summary>
        /// <param name="armour"></param>
        /// <returns></returns>
        public string EquipArmour(Armour armour)
        {
            List<Armour> armourFromList = Armours.FindAll(a => a != null && a.Slot == armour.Slot);
            if (armourFromList.Count >= 1)
            {
                return "An armour is already equipped in the " + armour.Slot + " slot. UnEquip first.";
            }
            else if (armour.GetLevel() > Level)
            {
                return "Equipping Armour failed. Hero's level is lower than armour's level";
            }
            else
            {
                switch (armour.Slot)
                {
                    case ArmourSlot.Body:
                        Armours[0] = armour;
                        break;

                    case ArmourSlot.Head:
                        Armours[1] = armour;

                        break;

                    case ArmourSlot.Legs:
                        Armours[2] = armour;
                        break;

                    default:
                        break;
                }
            }
            for (int i = 0; i < Armours.Count; i++)
            {
                if (Armours.ElementAtOrDefault(i) != null)
                {
                    Stats.Health += Armours[i].Stats.Health;
                    Stats.Strength += Armours[i].Stats.Strength;
                    Stats.Dexterity += Armours[i].Stats.Dexterity;
                    Stats.Intelligence += Armours[i].Stats.Intelligence;
                }
            }
            return "Armour successfully equipped";
        }

        /// <summary>
        /// Unequips armour and takes away the stats of that armour.
        /// </summary>
        /// <param name="armour"></param>
        public void UnEquipArmour(Armour armour)
        {
            //Checks whether the armour that is being unequipped was actually equipped.
            int index = Armours.IndexOf(armour);
            if (index != -1)
            {
                Stats.Health -= armour.Stats.Health;
                Stats.Strength -= armour.Stats.Strength;
                Stats.Dexterity -= armour.Stats.Dexterity;
                Stats.Intelligence -= armour.Stats.Intelligence;
                Armours[index] = null;
                Console.WriteLine("Armour successfully removed");
            }
            else
            {
                Console.WriteLine("Tried unequipping armour that was not equipped");
            }
        }

        /// <summary>
        /// Returns the body armour, if there is any. Otherwise returns feedback
        /// </summary>
        /// <returns></returns>
        public string GetBodyArmour()
        {
            if (Armours.ElementAtOrDefault(0) == null)
            {
                return "No body armour equipped";
            }
            return Armours[0].GetInfo();
        }

        /// <summary>
        /// Returns the head armour, if there is any. Otherwise returns feedback
        /// </summary>
        /// <returns></returns>
        public string GetHeadArmour()
        {
            if (Armours.ElementAtOrDefault(1) == null)
            {
                return "No head armour equipped";
            }
            return Armours[1].GetInfo();
        }

        /// <summary>
        /// Returns the leg armour, if there is any. Otherwise returns feedback
        /// </summary>
        /// <returns></returns>
        public string GetLegsArmour()
        {
            if (Armours.ElementAtOrDefault(2) == null)
            {
                return "No leg armour equipped";
            }
            return Armours[2].GetInfo();
        }

        #endregion Armour, Equip, Unequip, Get armours
    }
}