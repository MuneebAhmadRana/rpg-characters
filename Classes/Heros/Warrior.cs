﻿namespace RPGCharacters.Classes.Heros
{
    public class Warrior : Hero
    {
        public Warrior(string name, Stats stats) : base(name, stats)
        {
        }

        /// <summary>
        /// Level up stats for warrior.
        /// </summary>
        protected override void LevelUpStats()
        {
            base.Stats.Health += 30;
            base.Stats.Strength += 5;
            base.Stats.Dexterity += 2;
            base.Stats.Intelligence += 1;
        }
    }
}