﻿namespace RPGCharacters.Classes.Heros
{
    public class Mage : Hero
    {
        public Mage(string name, Stats stats) : base(name, stats)
        {
        }

        /// <summary>
        /// Level up stats for mage.
        /// </summary>
        protected override void LevelUpStats()
        {
            base.Stats.Health += 15;
            base.Stats.Strength += 1;
            base.Stats.Dexterity += 2;
            base.Stats.Intelligence += 5;
        }
    }
}