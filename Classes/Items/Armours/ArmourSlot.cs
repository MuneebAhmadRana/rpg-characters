﻿namespace RPGCharacters.Classes.Armours
{
    /// <summary>
    /// Slot enums for the different slots of armours.
    /// </summary>
    public enum ArmourSlot
    {
        Body,
        Head,
        Legs
    }
}