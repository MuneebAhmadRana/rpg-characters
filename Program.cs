﻿using RPGCharacters.Classes.Armours;
using RPGCharacters.Classes.Heros;
using RPGCharacters.Classes.Weapons;
using System;

namespace RPGCharacters
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("1. Creating new Warrior.");
            Warrior warrior = new Warrior("Nicholas Lennox", HeroBaseStats.GetWarriorStats());
            Console.WriteLine(warrior.GetHeroInfo());
            Console.WriteLine("-------------------");
            Console.WriteLine("2. Leveling it up to lvl 9"); 
            warrior.LevelUp(1300);
            Console.WriteLine(warrior.GetHeroInfo()); ;
            Console.WriteLine("-------------------");
            Console.WriteLine("3. Creating new weapon higher level than warrior. should not be able to equip");            
            Weapon bowOfLoneWolf = new Weapon("Long Bow of the Lone Wolf", 10, WeaponType.Ranged, WeaponBaseDamage.GetRangedBaseDamage());
            Console.WriteLine(warrior.EquipWeapon(bowOfLoneWolf));
            Console.WriteLine(warrior.GetHeroInfo());
            Console.WriteLine("-------------------");
            Console.WriteLine("4. Creating new weapon of same level as warrior and equipping it. Equipping a weapon after having another equipped will not work.");
            Weapon axeOfExiled = new Weapon("Great Axe of the Exiled", 5, WeaponType.Melee, WeaponBaseDamage.GetMeeleeBaseDamage());
            Console.WriteLine(warrior.EquipWeapon(axeOfExiled));
            Console.WriteLine(warrior.GetHeroInfo());
            Console.WriteLine("-------------------");
            Console.WriteLine("5. Adding armour alongside the weapon and showing warrior stats after");
            Armour plateChestJuggernaut = new Armour("Plate Chest of the Juggernaught", 5, ArmourType.Plate, ArmourSlot.Body, ArmourBaseStats.GetPlateArmourBaseStats());
            Console.WriteLine(warrior.EquipArmour(plateChestJuggernaut));
            Console.WriteLine(warrior.GetHeroInfo());
            Console.WriteLine("-------------------");
            Console.WriteLine("6. Attacking with current armour and weapon");
            Console.WriteLine("The warrior damage was: " + warrior.Attack());
            Console.WriteLine("-------------------");
            Console.WriteLine("7. Adding armour in a slot which is already equipped is not possible");
            Armour chestPlate = new Armour("A chest plate", 5, ArmourType.Plate, ArmourSlot.Body, ArmourBaseStats.GetPlateArmourBaseStats());
            Console.WriteLine(warrior.EquipArmour(chestPlate));
            Console.WriteLine("-------------------");
            Console.WriteLine("8. Unequipping armour takes away stats.");
            warrior.UnEquipArmour(plateChestJuggernaut);
            Console.WriteLine(warrior.GetHeroInfo());
            Console.WriteLine("-------------------");
            Console.WriteLine("9. Creating new armours and weapon and showing them on a new mage.");
            Mage mage = new Mage("Dewald Els", HeroBaseStats.GetMageStats());
            mage.LevelUp(650);
            Weapon javaScriptStaff = new Weapon("The JS Staff", 5, WeaponType.Magic, WeaponBaseDamage.GetMagicBaseDamage());
            Armour hat = new Armour("Ordinary Hat", 1, ArmourType.Cloth, ArmourSlot.Head, ArmourBaseStats.GetClothArmourBaseStats());
            Armour weakPlate = new Armour("Weak Plate", 1, ArmourType.Plate, ArmourSlot.Body, ArmourBaseStats.GetPlateArmourBaseStats());
            Armour leatherLeggings = new Armour("Patchy Leather Leggings", 2, ArmourType.Leather, ArmourSlot.Legs, ArmourBaseStats.GetLeatherArmourBaseStats());
            Console.WriteLine(mage.EquipWeapon(javaScriptStaff));
            Console.WriteLine(mage.EquipArmour(hat));
            Console.WriteLine(mage.EquipArmour(weakPlate));
            Console.WriteLine(mage.EquipArmour(leatherLeggings));
            Console.WriteLine(mage.GetHeroInfo());
            Console.WriteLine("The total attack damage: " + mage.Attack());
            Console.WriteLine("-------------------");
        }
    }
}