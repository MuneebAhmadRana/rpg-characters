﻿namespace RPGCharacters.Classes.Items
{
    /// <summary>
    /// Parent class for Weapon and Armour. 
    /// </summary>
    public abstract class Item
    {
        protected string Name { get; }
        protected int Level { get; }

        public Item(string name, int level)
        {
            Name = name;
            Level = level;
        }

        public int GetLevel()
        {
            return Level;
        }

        public abstract string GetInfo();
    }
}